# Vitest

This project is here to present [Vitest](https://vitest.dev/) and the migration from Jest to Vitest

## Pre-requisites

- [Node.js](https://nodejs.org/en/)


## Install project locally

To get project with installed dependancies, run following commands:

```bash
git clone git@gitlab.com:LucasVantommeUnis/vitest-demo-unis.git
cd vitest-demo-unit
npm install
```

## Launch unit tests with Jest

`npm run test`

## Migration to Vitest

`npm install vitest -D` 

-D to add this dependency as devDependency becayse it's not necessary for Prod

And we have to add a new script to `package.json` :

```bash
"scripts": {
    ...
    "vitest": "vitest --globals"
}
```